//
//  Style.swift
//  newsfeed
//
//  Created by Daniel Martin Jimenez on 4/4/17.
//  Copyright © 2017 Daniel Martin Jimenez. All rights reserved.
//

import Foundation
import UIKit

struct Style {
    
    //Colors
    static var firstColor = UIColor(red: 61/255, green: 181/255, blue: 189/255, alpha: 1.0)
    static var secondColor = UIColor(red: 0/255, green: 168/255, blue: 126/255, alpha: 1.0)
    static var thirdColor = UIColor(red: 252/255, green: 91/255, blue: 32/255, alpha: 1.0)
    
    //Fonts NFLogin
    static var fontTitleLogin = UIFont (name: "Avenir-Light", size: 50)
    static var fontPlaceholderLogin = UIFont (name: "Avenir-Bold", size: 14)
}
